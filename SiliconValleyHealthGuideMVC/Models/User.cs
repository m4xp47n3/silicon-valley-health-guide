﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiliconValleyHealthGuideMVC.Models
{
    public class User
    {
        public int id
        { get; set; }

        public String name
        { get; set; }

        public String username
        { get; set; }

        public String password
        { get; set; }

        public String type
        { get; set; }

        public String appointments
        { get; set; }

        public int hspId
        { get; set; }

    }
}