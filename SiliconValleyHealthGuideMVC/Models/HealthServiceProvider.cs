﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiliconValleyHealthGuideMVC.Models
{
    public class HealthServiceProvider
    {
        public int id
        { get; set; }

        public String name
        { get; set; }

        public String description
        { get; set; }

        public String contactInfo
        { get; set; }

        public String website
        { get; set; }

        public String services
        { get; set; }

        public String specializations
        { get; set; }

        public String hours
        { get; set; }

        public String appointments
        { get; set; }

        public String parking_facility
        { get; set; }

        public String special_annoucement
        { get; set; }

        public String rating
        { get; set; }

    }
}