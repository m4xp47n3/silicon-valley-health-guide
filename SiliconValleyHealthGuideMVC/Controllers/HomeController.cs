﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SiliconValleyHealthGuideMVC.Models;

namespace SiliconValleyHealthGuideMVC.Controllers
{
    public class HomeController : Controller
    {
        private SiliconValleyHealthGuideMVCContext db = new SiliconValleyHealthGuideMVCContext();

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View(db.HealthServiceProviders.ToList());
        }

        //
        // GET: /Home/Details/5

        public ActionResult Details(int id = 0)
        {
            HealthServiceProvider healthserviceprovider = db.HealthServiceProviders.Find(id);
            if (healthserviceprovider == null)
            {
                return HttpNotFound();
            }
            return View(healthserviceprovider);
        }

        //
        // GET: /Home/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Home/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HealthServiceProvider healthserviceprovider)
        {
            if (ModelState.IsValid)
            {
                db.HealthServiceProviders.Add(healthserviceprovider);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(healthserviceprovider);
        }

        //
        // GET: /Home/Edit/5

        public ActionResult Edit(int id = 0)
        {
            HealthServiceProvider healthserviceprovider = db.HealthServiceProviders.Find(id);
            if (healthserviceprovider == null)
            {
                return HttpNotFound();
            }
            return View(healthserviceprovider);
        }

        //
        // POST: /Home/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HealthServiceProvider healthserviceprovider)
        {
            if (ModelState.IsValid)
            {
                db.Entry(healthserviceprovider).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(healthserviceprovider);
        }

        //
        // GET: /Home/Delete/5

        public ActionResult Delete(int id = 0)
        {
            HealthServiceProvider healthserviceprovider = db.HealthServiceProviders.Find(id);
            if (healthserviceprovider == null)
            {
                return HttpNotFound();
            }
            return View(healthserviceprovider);
        }

        //
        // POST: /Home/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HealthServiceProvider healthserviceprovider = db.HealthServiceProviders.Find(id);
            db.HealthServiceProviders.Remove(healthserviceprovider);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}