﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using SiliconValleyHealthGuideMVC.Models;

namespace SiliconValleyHealthGuideMVC.Controllers
{
    public class HealthServiceProviderController : ApiController
    {
        private SiliconValleyHealthGuideMVCContext db = new SiliconValleyHealthGuideMVCContext();

        [Queryable]
        // GET api/HealthServiceProvider
        public IQueryable<HealthServiceProvider> GetHealthServiceProviders()
        {
            return db.HealthServiceProviders.AsQueryable();
        }

        // GET api/HealthServiceProvider/5
        public HealthServiceProvider GetHealthServiceProvider(int id)
        {
            HealthServiceProvider healthserviceprovider = db.HealthServiceProviders.Find(id);
            if (healthserviceprovider == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return healthserviceprovider;
        }

        // PUT api/HealthServiceProvider/5
        public HttpResponseMessage PutHealthServiceProvider(int id, HealthServiceProvider healthserviceprovider)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != healthserviceprovider.id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(healthserviceprovider).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/HealthServiceProvider/5
        public HttpResponseMessage PostHealthServiceProvider(int id, HealthServiceProvider healthserviceprovider)
        {
            if (ModelState.IsValid)
            {
                db.HealthServiceProviders.Add(healthserviceprovider);
                db.SaveChanges();
                db.Users.Find(id).hspId = healthserviceprovider.id;
                db.SaveChanges();
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, healthserviceprovider);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = healthserviceprovider.id }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/HealthServiceProvider/5
        public HttpResponseMessage DeleteHealthServiceProvider(int id)
        {
            HealthServiceProvider healthserviceprovider = db.HealthServiceProviders.Find(id);
            if (healthserviceprovider == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.HealthServiceProviders.Remove(healthserviceprovider);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, healthserviceprovider);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}