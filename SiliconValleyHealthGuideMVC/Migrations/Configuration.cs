namespace SiliconValleyHealthGuideMVC.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using SiliconValleyHealthGuideMVC.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<SiliconValleyHealthGuideMVC.Models.SiliconValleyHealthGuideMVCContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(SiliconValleyHealthGuideMVC.Models.SiliconValleyHealthGuideMVCContext context)
        {
            context.Users.AddOrUpdate(
                u => u.name,

                new User { name="Siddhi", username="siddhi.chogle", password="chogle", type="user"}
            );

            context.HealthServiceProviders.AddOrUpdate(
                p => p.name,

                new HealthServiceProvider
                {
                    name = "Santa Clara Health Center",
                    description = "Your health our Concern!",
                    services = "Free Diagnosis, Discounted Rates for Senior Citizens",
                    contactInfo = "Tel: 408-354-0000; Address: 101 Santa Clara St, San Jose, CA 95112",
                    hours = "9am - 9pm",
                    specializations = "Orthopedic Surgeries",
                    website = "www.santaclarahealthcenter.com"
                },

                new HealthServiceProvider
                {
                    name = "Saint James Health Care",
                    description = "The best Health Care in Silicon Valley",
                    services = "Discounted Rates for Senior Citizens",
                    contactInfo = "Tel: 408-555-0000; Address: 101 Saint James St, San Jose, CA 95112",
                    hours = "24 Hours",
                    specializations = "Heart Surgeries",
                    website = "www.saintjameshealthcenter.com"
                },

                new HealthServiceProvider
                {
                    name = "San Pedro Hospital",
                    description = "The most popular hospital in San Jose",
                    services = "Discounted Rates for Children",
                    contactInfo = "Tel: 408-555-0000; Address: 101 San Pedro Sq, San Jose, CA 95112",
                    hours = "24 Hours",
                    specializations = "Pediatrician",
                    website = "www.sanpedrohospital.com"
                }
             );
        }
    }
}
